# Oh12306
<img src="./entry/src/main/resources/base/media/icon.png" width="100" alt="oh12306_icon" />

## 介绍
一款非官方 | 铁路12306 | OpenHarmony移植版。

## 图标版权
本仓库所有图标均由 [@ericple](https://gitee.com/ericple) 绘制

## 开发环境

- DevEco 4.0.0.400 Beta2
- Compile SDK 3.1.0 API9
- node npm-16.20.1

## 编译前注意

- 为了在不同系统上保持最好的显示效果，避免状态栏入侵，请注意：
  - 对于Harmony OS 3.1/4.0或以上设备，请将EntryAbility内onCreate方法中的globalThis.universalAppBarTopPadding改为 32
  - 对于OpenHarmony OS设备，要进一步实现状态栏沉浸效果，请将上一点内提到的变量改为0（默认为0）

## TODO

- [x] 车票搜索
- [x] 车站搜索
- [x] 用户登录
- [x] 订单查询
- [ ] 车票预定
- [ ] 时刻表查询
- [ ] ...

## 应用截图

<img src="./ScreenShots/1.png" width="200" alt="stations" />
<img src="./ScreenShots/2.png" width="200" alt="results" />

<img src="./ScreenShots/3.png" width="200" alt="err-handle" />
<img src="./ScreenShots/4.png" width="200" alt="results" />
<img src="./ScreenShots/5.png" width="200" alt="results" />
<img src="./ScreenShots/6.png" width="200" alt="results" />

## 关于OH12306

本仓库**不属于**中国铁道科学研究院集团有限公司，所使用的API均为对12306网站分析所得，仅供学习交流使用，
禁止用于商业用途，为防止不法用途，API已打包闭源。禁止将本应用与12306官方应用混淆。
仓库归[@ericple](https://gitee.com/ericple)所有。

## 打赏

如果您认为本仓库内容对您有价值，可以在此处对我**打赏**。此项并非必须，但其对我的鼓励将得以让本仓库获得持续更新。

<img src="./ScreenShots/qrc-ali.jpg" width="128" alt="results" />
<img src="./ScreenShots/qrc-wec.jpg" width="128" alt="results" />
