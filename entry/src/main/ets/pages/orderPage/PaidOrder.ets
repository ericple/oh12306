// Guo Tingjin dev@peercat.cn
//
// 本程序是自由软件：你可以再分发之和/或依照由自由软件基金会发布的 GNU 通用公共许可证修改之，无论是版本 3 许可证，还是（按你的决定）任何以后版都可以。
//
// 发布该程序是希望它能有用，但是并无保障;甚至连可销售和符合某个特定的目的都不保证。请参看 GNU 通用公共许可证，了解详情。
//
// 你应该随程序获得一份 GNU 通用公共许可证的复本。如果没有，请看 <https://www.gnu.org/licenses/>。

import { UniversalAppBar } from '../../components/UniversalAppBar';
import { EndPoints, ApiMan, CookieMan, Responses, StringUtils } from '@ohos/lib12306';
import router from '@ohos.router';
import promptAction from '@ohos.promptAction';

@Component
@Entry
struct PaidOrder {
  @State activeTabIndex: number = 0;
  @State hasOrder: boolean = false;
  @State queryType: 'G' | 'H' = 'G';
  @State orderNumber: string = '0';
  @State orderResponse: Responses.QueryOrderResponse = undefined;
  @State Cookie: string = '';

  @Builder
  QueryTypeSelector(title: ResourceStr, queryType: "G" | "H") {
    Text(title)
      .fontSize(this.queryType == queryType ? 13 : 12)
      .fontColor(Color.White)
      .margin({ left: 12, top: 4, bottom: 8 })
      .padding({ bottom: 4 })
      .onClick(() => {
        this.queryType = queryType;
        this.Cookie = CookieMan.getInstance().parse();
        ApiMan.getInstance().requestUserOrder({
          queryWhere: this.queryType,
          cookies: CookieMan.getInstance().parse()
        }).then(orderResponse => {
          this.hasOrder = (orderResponse.orderTotalNumber && orderResponse.orderTotalNumber != '0');
          if (!this.hasOrder) {
            return;
          }
          this.orderResponse = orderResponse;
          this.orderNumber = orderResponse.orderTotalNumber;
        })
      })
      .border({ color: Color.White, width: { bottom: this.queryType == queryType ? 2 : 0 } })
  }

  aboutToAppear() {
    if (!CookieMan.getInstance().get('tk')) {
      router.pushUrl({ url: 'pages/Login' });
      promptAction.showToast({ message: '请先登录!' });
    } else {
      ApiMan.getInstance().requestUserOrder({
        queryWhere: this.queryType,
        cookies: CookieMan.getInstance().parse()
      }).then(orderResponse => {
        this.hasOrder = (orderResponse.orderTotalNumber && orderResponse.orderTotalNumber != '0');
        if (!this.hasOrder) {
          return;
        }
        this.orderResponse = orderResponse;
        this.orderNumber = orderResponse.orderTotalNumber;
      });
    }
  }

  build() {
    Column() {
      UniversalAppBar({
        title: "已支付",
        routerOptions: {
          url: "pages/Main",
          params: {
            tabIndex: 2
          }
        }
      })
      Row() {
        this.QueryTypeSelector("未出行订单", "G")
        this.QueryTypeSelector("历史订单", "H")
      }.backgroundColor($r("app.color.secondary_blue")).width('100%').justifyContent(FlexAlign.Start)

      if (!this.hasOrder) {
        Column() {
          Row() {
            Image(EndPoints.orderEmptyImage)
              .size({ width: 72, height: 72 })
          }.margin({ bottom: 16 })

          Text("没有查到您的订单信息")
            .fontSize(14)
            .fontColor(Color.Gray)
        }.margin({ top: '50%' })
      } else {
        List() {
          ForEach(this.orderResponse.OrderDTODataList, order => {
            ListItem() {
              Column() {
                Row() {
                  Text("下单日期：")
                    .fontSize(10)
                    .fontColor(Color.Orange)
                  Text(StringUtils.formatDateByObj(new Date(order.order_date), "Y年M月D日"))
                    .fontSize(10)
                    .fontColor(Color.Orange)
                }
                .height(24)
                .borderRadius(8)
                .backgroundColor($r("app.color.order_date_font_color"))
                .margin({ bottom: 12 })
                .padding(4)

                Row() {
                  Column() {
                    Row() {
                      Image($r("app.media.ic_train_filter_direct"))
                        .size({width: 14, height: 14})
                        .margin({left: 12, right: 12})
                      Text(`${order.tickets[0].stationTrainDTO.station_train_code}   ${order.tickets[0].stationTrainDTO.from_station_name} - ${order.tickets[0].stationTrainDTO.to_station_name}`)
                        .fontColor($r("app.color.order_card_font_color"))
                        .fontSize(14)
                    }
                    .width('100%')
                    .height(36)
                    .backgroundColor($r("app.color.order_card_head_color"))
                    .borderRadius({topLeft: 6, topRight: 6})

                    Column() {
                      Text(`订 单 号：${order.sequence_no}`)
                        .fontColor($r("app.color.order_card_font_color"))
                        .fontSize(12)
                        .margin({ bottom: 8 })
                        .width('100%')
                      Text(`发车时间：${order.start_train_date_page}开`)
                        .fontColor($r("app.color.order_card_font_color"))
                        .fontSize(12)
                        .margin({ bottom: 8 })
                        .width('100%')
                      Row() {
                        Text(`总 张 数：${order.tickets.length}张 `)
                          .fontColor($r("app.color.order_card_font_color"))
                          .fontSize(12)
                          .margin({ bottom: 16 })
                        ForEach(order.array_passser_name_page, passenger => {
                          Text(passenger).margin({ left: 12 })
                            .fontColor($r("app.color.order_card_font_color"))
                            .fontSize(12)
                            .margin({ bottom: 16 })
                        })
                      }.width('100%')
                    }.width('100%').backgroundColor($r("app.color.order_card_body_color")).borderRadius({bottomLeft: 6, bottomRight: 6}).padding({left: 28, top: 16})
                  }
                }.width('100%')
              }.width('100%')
            }.padding(12)
          })
        }.listDirection(Axis.Vertical).height(globalThis.displayHeight - 31 - globalThis.universalAppBarTopPadding)
      }
    }.backgroundColor($r("app.color.main_bg_color"))
  }
}