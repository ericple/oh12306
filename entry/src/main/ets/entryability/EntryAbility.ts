import AbilityConstant from '@ohos.app.ability.AbilityConstant';
import hilog from '@ohos.hilog';
import UIAbility from '@ohos.app.ability.UIAbility';
import Want from '@ohos.app.ability.Want';
import window from '@ohos.window';
import { CookieMan } from '@ohos/lib12306';
import { ApiMan } from '@ohos/lib12306';
import { TaskMan } from '@ohos/lib12306';
import display from '@ohos.display';

export default class EntryAbility extends UIAbility {
  onCreate(want: Want, launchParam: AbilityConstant.LaunchParam) {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
    globalThis.appContext = this.context;
    globalThis.cookieMan = new CookieMan();
    globalThis.apiMan = new ApiMan();
    globalThis.taskMan = new TaskMan();
    // 为防止侵入statusBar，设置一个Padding，作用类似于SafeAreaView
    // Harmony OS 推荐值为32
    // Open Harmony要进行沉浸适配，则将此值修改为0
    globalThis.universalAppBarTopPadding = 0;
    const displayInfo = display.getDefaultDisplaySync();
    // 获取屏幕高度 OpenHarmony / 模拟器
    globalThis.displayHeight = displayInfo.height / displayInfo.densityPixels;
  }

  onDestroy() {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    // Main window is created, set main page for this ability
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
    windowStage.loadContent('pages/Splash', (err, data) => {
      if (err.code) {
        hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '');
        return;
      }
      hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', JSON.stringify(data) ?? '');
    });
  }

  onWindowStageDestroy() {
    // Main window is destroyed, release UI related resources
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
  }

  onForeground() {
    // Ability has brought to foreground
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
  }

  onBackground() {
    // Ability has back to background
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
  }
}
